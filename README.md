# README #

### What is this repository for? ###

* This repo holds the codebase for the registry health checker process. This queries this link: https://registry.aunalytics.com/api/health and makes sure that all the registries are working properly.

### How do I get set up? ###

To run this locally:

* docker build -t <whatever_you_wanna_call_it> -f Dockerfile .