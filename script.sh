#!/bin/bash
# Auto generated code


python main.py

#$? means the return value of the python script.
if [ $? -eq 0 ] ; then
  echo 'SUCCEEDED'
  exit 0
else
  echo 'FAILED'
  exit $?
fi
