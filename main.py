# Auto-generated code

import traceback
import utils
import sys
import urllib.request, json
import os
logger = utils.get_logger(__name__)
context = utils.context


def main():

    #url to get registry health :)
    logger.info("starting registry health process.")
    url_string = 'https://registrymt.aunalytics.com/api/health'
    data = "" #variable to store data
    error_string=""
    with urllib.request.urlopen(url_string) as url:
        data = json.loads(url.read().decode())
    components = (data["components"]);

    #loop through components from data output from URL. See if we can't find any unhealthy
    #registries. If we find them, record an error.
    for component in components:
        name = component["name"];
        if name != "clair": #we don't care about clair rn
            status = component["status"]
            if status != "healthy":
                error_string += component["error"] + "\n";

    if error_string != "":
        logger.error("Errors found: "+ error_string)
        sys.exit(1)
    else:
        logger.info("All registries functioning properly.");
        sys.exit(0)
    pass

try:
    # Wrapping it around exception to catch all the errors
    # to send the logs to logger stream
    main()
except Exception as e:
    logger.error(traceback.format_exc())
    raise
